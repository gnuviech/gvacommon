# -*- coding: utf-8 -*-
#
# gvacommon - common parts of gnuviechadmin
# Copyright (C) 2016-2018  Jan Dittberner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import absolute_import, unicode_literals

from unittest import TestCase

import os
from django.core.exceptions import ImproperlyConfigured

from gvacommon.settings_utils import get_env_variable


class GetEnvVariableTest(TestCase):
    def test_get_existing_env_variable(self):
        os.environ['testvariable'] = 'myvalue'
        self.assertEqual(get_env_variable('testvariable'), 'myvalue')

    def test_get_missing_env_variable(self):
        if 'missingvariable' in os.environ:
            del os.environ['missingvariable']
        with self.assertRaises(ImproperlyConfigured) as e:
            get_env_variable('missingvariable')
        self.assertEqual(
            str(e.exception), 'Set the missingvariable environment variable')

    def test_typed_variable(self):
        os.environ['testvariable'] = '42'
        self.assertEqual(get_env_variable('testvariable'), '42')
        self.assertEqual(get_env_variable('testvariable', int), 42)
        self.assertIsInstance(get_env_variable('testvariable', int), int)

    def test_get_default_for_missing_variable(self):
        if 'missingvariable' in os.environ:
            del os.environ['missingvariable']
        self.assertEqual(get_env_variable(
            'missingvariable', default='test'), 'test')
