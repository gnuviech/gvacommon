#
# gvacommon - common parts of gnuviechadmin
# Copyright (C) 2014-2018  Jan Dittberner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Tests for gvacommon code.

"""
import unittest
from unittest.mock import MagicMock

import django
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import connection
from django.http import HttpResponseForbidden
from django.views.generic import View

if not settings.configured:
    settings.configure(
        DEBUG=True,
        SECRET_KEY="unused#secret",
        ROOT_URLCONF='tests',
        TEMPLATE_DIRS=['.'],
        DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'test',
            }
        },
        INSTALLED_APPS=(
            'django.contrib.contenttypes',
            'django.contrib.auth',),
    )

django.setup()

urlpatterns = []


class StaffOrSelfLoginRequiredMixinTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        connection.creation.create_test_db()
        User = get_user_model()
        User.objects.create_superuser(
            username='admin', password='admin', email='test@example.org')
        User.objects.create_user(username='test')

    @classmethod
    def tearDownClass(cls):
        connection.creation.destroy_test_db()

    def setUp(self):
        from gvacommon.viewmixins import StaffOrSelfLoginRequiredMixin

        class SubjectView(StaffOrSelfLoginRequiredMixin, View):
            def get(self, request, *args, **kwargs):
                return "success"

            def get_customer_object(self):
                return get_user_model().objects.get(username='test')

        self.subject = SubjectView.as_view()

    def test_dispatch_anonymous(self):
        from django.contrib.auth.models import AnonymousUser
        request = MagicMock(method='GET')
        request.user = AnonymousUser()
        result = self.subject(request)
        self.assertIsInstance(result, HttpResponseForbidden)

    def test_dispatch_staff_user(self):
        request = MagicMock(method='GET')
        request.user = get_user_model().objects.get(username='admin')
        result = self.subject(request)
        self.assertEqual(result, "success")

    def test_dispatch_customer_user(self):
        request = MagicMock(method='GET')
        request.user = get_user_model().objects.get(username='test')
        result = self.subject(request)
        self.assertEqual(result, "success")

    def test_dispatch_other_user(self):
        request = MagicMock(method='GET')
        request.user = get_user_model().objects.create_user('other')
        result = self.subject(request)
        self.assertIsInstance(result, HttpResponseForbidden)

    def test_get_customer_object_not_implemented(self):
        from gvacommon.viewmixins import StaffOrSelfLoginRequiredMixin

        class IncompleteView(StaffOrSelfLoginRequiredMixin, View):
            pass

        view = IncompleteView()
        with self.assertRaises(NotImplementedError):
            view.get_customer_object()
