#
# gvacommon - common parts of gnuviechadmin
# Copyright (C) 2015-2023  Jan Dittberner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
This module defines mixins for gnuviechadmin views.

"""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.utils.translation import gettext as _


class StaffOrSelfLoginRequiredMixin(LoginRequiredMixin):
    """
    Mixin that makes sure that a user is logged in and matches the current
    customer or is a staff user.

    """

    def dispatch(self, request, *args, **kwargs):
        if (
                request.user.is_staff or
                request.user == self.get_customer_object()
        ):
            return super(StaffOrSelfLoginRequiredMixin, self).dispatch(
                request, *args, **kwargs
            )
        return HttpResponseForbidden(
            _('You are not allowed to view this page.')
        )

    def get_customer_object(self):
        """
        Views based on this mixin have to implement this method to return
        the customer that must be an object of the same class as the
        django.contrib.auth user type.

        :return: customer
        :rtype: settings.AUTH_USER_MODEL

        """
        raise NotImplementedError(
            "subclass has to implement get_customer_object")
