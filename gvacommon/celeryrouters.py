#
# gvacommon - common parts of gnuviechadmin
# Copyright (C) 2015-2016  Jan Dittberner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import unicode_literals


class GvaRouter(object):
    def route_for_task(self, task, args=None, kwargs=None):
        for route in ['ldap', 'file', 'mysql', 'pgsql', 'web']:
            if route in task:
                return {
                    'exchange': route,
                    'exchange_type': 'direct',
                    'queue': route,
                }
        return None
