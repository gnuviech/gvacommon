#
# gvacommon - common parts of gnuviechadmin
# Copyright (C) 2016-2018  Jan Dittberner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.core.exceptions import ImproperlyConfigured
from os import environ


def get_env_variable(var_name, type=str, default=None):
    """
    Get a setting from an environment variable.

    :param str var_name: variable name
    :raises ImproperlyConfigured: if the environment setting is not defined
    :return: environment setting value
    :rtype: str
    """
    try:
        return type(environ[var_name])
    except KeyError:
        if default is not None:
            return default
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)
