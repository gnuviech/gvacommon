=========
gvacommon
=========

This module contains code that is intended to be shared between gnuviechadmin
components.

Install dependencies
====================

.. code-block::

   poetry install --with=dev

Run tests
=========

To run the unit tests and report test coverage use:

.. code-block::

   poetry run tox
